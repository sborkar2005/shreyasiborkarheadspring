﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using HeadSpringsDatabase.Models;
using HeadSpringsDatabase.EmpDatabase;
using HeadSpringsDatabase.Abstraction;


namespace HeadSpringsDatabase.Controllers
{
    public class EmployeeController : Controller
    {
        // Employee Database
        private IRepository EmployeeRepository;

        public EmployeeController()
        {
            this.EmployeeRepository = new EmployeeRepository(new EmployeeContext());
        }

        public EmployeeController(IRepository EmployeeRepository)
        {
            this.EmployeeRepository = EmployeeRepository;
        }
        //
        // GET: /Employee/
        [Authorize]
        public ActionResult Index()
        {
            try
            {

                var empList = from s in EmployeeRepository.GetAllEmployeess() select s;

                return View(empList);
            }

            catch (Exception)
            {
                ModelState.AddModelError("", "An error occurred. We are working hard to resolve this.");
            }

            return null;
        }

        //
        // GET: /Employee/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            try
            {
                EmployeeDetails employee = EmployeeRepository.GetByEmpNumber(id);

                if (employee == null)
                {
                    return HttpNotFound();
                }
                return View(employee);
            }

            catch (Exception)
            {
                ModelState.AddModelError("", "An error occurred. We are working hard to resolve this.");
            }

            return View();

        }

        //
        // GET: /Employee/Create

        public ActionResult Create()
        {
            CreateDropDownLists();

            return View();
        }

        //
        // POST: /Employee/Create

        [Authorize(Roles = "HR")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeDetails employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CreateDropDownLists();
                    //employee.UserId = 2;
                    EmployeeRepository.CreateNewEmployee(employee);
                    EmployeeRepository.saveEmployee();

                    var user = Membership.GetUser(employee.UserName);

                    if (!Roles.IsUserInRole(employee.UserName, employee.Role) && user == null)
                    {
                        WebSecurity.CreateUserAndAccount(employee.UserName, employee.UserName);
                        Roles.AddUserToRole(employee.UserName, employee.Role);

                    }

                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "An error occurred. We are working hard to resolve this.");
            }


            return View(employee);
        }



        //
        // GET: /Employee/Edit/5

        public ActionResult Edit(int id = 0)
        {

            EmployeeDetails employee = EmployeeRepository.GetByEmpNumber(id);
            // CreateDropDownLists(employee.Role);

            if (employee == null)
            {
                return HttpNotFound();
            }

            return View(employee);
        }

        //
        // POST: /Employee/Edit/5
        [Authorize(Roles = "HR")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeDetails employee)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    //Get the roles Dropdownlist 
                    //   CreateDropDownLists(employee.Role);
                    EmployeeDetails Emp = EmployeeRepository.GetByEmpNumber(employee.UserId);
                    if (Emp.Role != null && !(employee.Role == Emp.Role) && Roles.RoleExists(employee.Role) && Roles.RoleExists(Emp.Role) && Roles.IsUserInRole(employee.UserName, Emp.Role)) Roles.RemoveUserFromRole(employee.UserName, Emp.Role);
                    EmployeeRepository.ModifyEmployee(employee);
                    EmployeeRepository.saveEmployee();

                    // This is being done because the user is created by the HR person and has not registered through the normal
                    // Login screen
                    var user = Membership.GetUser(employee.UserName);
                    if (!Roles.IsUserInRole(employee.UserName, employee.Role) && user != null && employee.Role != null && Roles.RoleExists(employee.Role))
                    {
                        Roles.AddUserToRole(employee.UserName, employee.Role);

                    }
                    else if (!Roles.IsUserInRole(employee.UserName, employee.Role) && Roles.RoleExists(employee.Role))
                    {
                        WebSecurity.CreateUserAndAccount(employee.UserName, employee.UserName);
                        Roles.AddUserToRole(employee.UserName, employee.Role);

                    }

                    else
                    {
                        ModelState.AddModelError("", "Unable to edit employee role data. The error has been sent to the System Administrator.");

                    }

                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to edit employee. The error has been sent to the System Administrator.");
            }


            return View(employee);
        }

        //
        // GET: /Employee/Delete/5

        public ActionResult Delete(int id = 0)
        {
            EmployeeDetails employee = EmployeeRepository.GetByEmpNumber(id);

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        //
        // POST: /Employee/Delete/5
        [Authorize(Roles = "HR")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeDetails employee = EmployeeRepository.GetByEmpNumber(id);

                    // Delete them from the Roles and Membership tables before deleting them from the EmpDetails table
                    if (Roles.IsUserInRole(employee.UserName, employee.Role))
                        Roles.RemoveUserFromRole(employee.UserName, employee.Role);
                    Membership.DeleteUser(employee.UserName);
                    EmployeeRepository.RemoveEmployee(id);
                    EmployeeRepository.saveEmployee();


                    return RedirectToAction("Index");
                }
            }

            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to delete employee. The error has been sent to the System Administrator.");
            }


            return View();
        }

        /// <summary>
        /// Search through the employee database
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public ActionResult SearchIndex(string searchString)
        {
            try
            {
                var empList = from person in EmployeeRepository.GetAllEmployeess() select person;
                if (!String.IsNullOrEmpty(searchString))
                {
                    empList = empList.Where(emp => emp.FullName.ToUpper().Contains(searchString.ToUpper())||
                                                emp.FullName.ToLower().Contains(searchString.ToLower()));
                }
                ViewBag.TotalCount = empList.Count();

                return View(empList);
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to search employee. The error has been sent to the System Administrator.");
            }


            return View();

        }

        protected override void Dispose(bool disposing)
        {
            EmployeeRepository.Dispose();
            base.Dispose(disposing);
        }



        // This method creates the Roles Dropdownlist
        private void CreateDropDownLists(object role = null)
        {

            try
            {
                List<SelectListItem> li = new List<SelectListItem>();


                foreach (string rolename in Roles.GetAllRoles())
                {
                    if (role != null)
                        li.Add(new SelectListItem { Text = rolename, Value = rolename });
                    else
                        li.Add(new SelectListItem { Text = rolename, Value = rolename });
                }

                ViewData["Role"] = li.ToList();
                ViewBag.Roles = li;
            }

            catch (DataException ex)
            {
                ModelState.AddModelError("", ex.ToString());
            }

        }
    }
}