﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace HeadSpringsDatabase.Models
{

    

     [Table("EmployeeDetails")]
    public class EmployeeDetails
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [RegularExpression(@"([a-z0-9]){8}$", ErrorMessage = "Username is required and should be 8 characters long and cannot have upper case and special characters.")]
        [Display(Name = "User name")]
        public string UserName { get; set; }
        
        [Display(Name = "Full name")]
        [RegularExpression(@"([A-Za-z0-9\s\-]){2,100}$", ErrorMessage = "Full Name is required. Atleast 2 characters long.")]
        public string FullName { get; set; }
         
         [Display(Name = "Job Title")]

         public string JobTitle { get; set; }

         public string Location { get; set; }

         [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                            ErrorMessage = "Email is not valid")]
         [Display(Name = "Email Address")]
         public string Email { get; set; }

         [Display(Name = "Phone Number")]
         [DataType(DataType.PhoneNumber)]
         [RegularExpression(@"^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$", ErrorMessage = "Please enter proper phone number in xxx-xxx-xxxx format.")]
         [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:###-###-####}")]
         public string PhoneNumber { get; set; }

         [Display(Name = "Role Name")]
         [DataType(DataType.Text)]
         public string Role { get; set; }

    }


}