﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using HeadSpringsDatabase.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace HeadSpringsDatabase.EmpDatabase
{
    public class EmployeeContext : DbContext
    {
        public EmployeeContext()
            : base("EmployeeDatabase")
        {

        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<EmployeeDetails> Employees { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}