﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using HeadSpringsDatabase.Models;
using HeadSpringsDatabase.Abstraction;
using System.Data.Entity.ModelConfiguration.Conventions;

using HeadSpringsDatabase.EmpDatabase;

namespace HeadSpringsDatabase.EmpDatabase
{
    public class EmployeeInitializer : DropCreateDatabaseIfModelChanges<EmployeeContext>
    {
        protected override void Seed(EmployeeContext context)
        {

            var _employee = new List<EmployeeDetails>()
            {
                new EmployeeDetails {UserId=1, UserName="Test", FullName="Test Test",  Location ="Test", Email="test@gmail.con", JobTitle="IT Test", PhoneNumber="501-246-0134", Role="Admin" }
         
                        
            };
            _employee.ForEach(x => context.Employees.Add(x));
            context.SaveChanges();
        }
    }
}