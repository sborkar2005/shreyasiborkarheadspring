﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HeadSpringsDatabase.Models;

namespace HeadSpringsDatabase.Abstraction
{
    // Interface to handle all Employee Related CRUD functions
    public interface IRepository : IDisposable
    {
        IEnumerable<EmployeeDetails> GetAllEmployeess();
        EmployeeDetails GetByEmpNumber(int EmpId);
        bool GetByEmpName(string EmpName);
        void CreateNewEmployee(EmployeeDetails employee);
        void RemoveEmployee(int EmpId);
        void ModifyEmployee(EmployeeDetails employee);
        void saveEmployee();
    }
}