﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using HeadSpringsDatabase.Models;
using HeadSpringsDatabase.EmpDatabase;
using System.Web.Security;


namespace HeadSpringsDatabase.Abstraction
{
    // Concrete class to handle Employee CRUD functions
    public class EmployeeRepository : IRepository, IDisposable
    {
        private EmployeeContext context;

        public EmployeeRepository(EmployeeContext context)
        {
            this.context = context;
        }


        public IEnumerable<EmployeeDetails> GetAllEmployeess()
        {
            try
            { return context.Employees.ToList(); }
             catch (Exception ex)
            {
                throw new Exception("Error", ex);
               
            }        
        }

        

        public EmployeeDetails GetByEmpNumber(int EmpId)
        {

            try
            { return context.Employees.Find(EmpId); }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);

            }
            
        }

        public bool GetByEmpName(string EmpName)
        {
            try
            {
                bool returnVal = false;
                var emps = from per in context.Employees select per;
                emps = emps.Where(emp => emp.UserName.Contains(EmpName));
                if (emps.Count() > 0) returnVal = true;
                return returnVal;
            }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);

            }

        }

        public void CreateNewEmployee(EmployeeDetails employee)
        {
            try
            { context.Employees.Add(employee); }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);
            }
            
        }

        public void RemoveEmployee(int EmpId)
        {
            try
            {
                EmployeeDetails employee = context.Employees.Find(EmpId);
                context.Employees.Remove(employee);
            }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);
            }

        }

        public void ModifyEmployee(EmployeeDetails employee)
        {

            var oldval = context.Employees.Find(employee.UserId);
            context.Entry(oldval).CurrentValues.SetValues(employee);
        }

        public void saveEmployee()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);
            }
            
        }

        private bool ContextDisposed = false;

        protected virtual void Dispose(bool disposal)
        {
            try
            {
                if (!this.ContextDisposed)
                {
                    if (disposal)
                    {
                        context.Dispose();
                    }
                }
                this.ContextDisposed = true;
            }

            catch (Exception ex)
            {
                throw new Exception("Error", ex);
            }
        }

        public void Dispose()
        {
            try
            {

                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);
            }
           
        }

      
    }
}
